import mongoose from "mongoose";
import validators from 'mongoose-validators'

export const OrgMemberSch = new mongoose.Schema({
    orgId: { type: mongoose.Schema.Types.ObjectId, ref: 'Organization', required: [true, "Please provide your userId"] },
    newMember: { type: String, required: [true, "Fill the newMember username"] },
    role: {
        type: String,
        enum: ['ADM', 'MNG', 'NOU', 'TLD', 'TEM', 'CLT', 'EXE'],
        required: [true, "Please provide the role"],
        default: "TEM"
    },
    assignedBy: { type: String, required: [true, "Please provide the assignedBy"] },
    status: {
        type: String,
        enum: ['WOK', 'RMV', 'TRS'],
        required: [true, "Please provide the status"],
        default: "WOK"
    },
    creation_date: { type: Date, default: Date.now },
    username: { type: String },
    Modified_date: { type: Date, default: Date.now }
})

export default mongoose.model('OrgMember', OrgMemberSch)