import mongoose from "mongoose";

export const UserSchema = new mongoose.Schema({
    username:{
        type:String,
        required:[true,"Please provide unique username"],
        unique:[true,"Username already exist"]
    }, 
    password:{
        type:String
    },
    email:{
        type:String,
        required:[true,"Please provide email"],
        unique:[true,"Email already exist"] 
    }, 
    isExternal:{      // if true-> Ext user, false -> Normal user
        type:Boolean,
        required:[true,"Provide isExternal"],
        default:false
    },
    mobile:{type:Number},
    details:{type:String}
}) 

export default mongoose.model('User',UserSchema)