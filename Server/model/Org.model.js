import mongoose from "mongoose";
import validators from "mongoose-validators";

export const OrgSchema = new mongoose.Schema({
    orgName: {
        type: String,
        required: [true, "Please provide your organization name"]
    },
    address: { type: String, required: [true, "Please provide your address"] },
    pincode: { type: Number, validate: validators.isLength(6), required: [true, "Please provide your zip-code"] },
    website: { type: String, validate: validators.isURL() },
    phone: { type: String },//+91-xxxxx xxxxx
    email: {
        type: String,
        required: [true, "Please provide email"],
        validate: validators.isEmail()
    },
    status: {
        type: String,
        enum: ['WOK', 'RMV', 'TRS'],
        required: [true, "Please provide the status properly"]
    },
    created_by: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: [true, "Please provide your userId"] },
    guestId: { type: mongoose.Schema.Types.ObjectId, ref: 'Organization', default: null },
    Creation_date: { type: Date, default: Date.now }
})

export default mongoose.model('Organization', OrgSchema)