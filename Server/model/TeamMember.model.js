import mongoose from "mongoose";
import validators from 'mongoose-validators'

export const TeamMemberSch = new mongoose.Schema({
    username: { type: String, required: [true, "Fill the field for new member username"]},
    TeamId: { type: String, required: [true, "Please provide your Team Id"], },
    Role: {
        type: String,
        enum: ['ADM', 'MNG', 'NOU', 'TLD', 'TEM', 'CLT', 'EXE'],
        required:[true,"Please provide the role"]
    },
    assignedBy: { type: String,required:[true,"Please provide the assignedBy"]},
    status: {
        type: String,
        enum: ['WOK', 'RMV', 'TRS'],
        required:[true,"Please provide the status"]
    },
    Creation_date: { type: Date, default: Date.now },
    userModified: { type: String },
    Modified_date: { type: Date, default: Date.now  }
})

export default mongoose.model('TeamMember', TeamMemberSch) 