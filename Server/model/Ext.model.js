import mongoose from "mongoose";
import validators from 'mongoose-validators'
const ExtSchema = new mongoose.Schema({
    email: {
        type: String,
        required: [true, "Please provide email"],
        validate: validators.isEmail()
    },
    extName:{
        type: String,
        required: [true, "Please provide username"],
        unique:[true,"Username should be unique"]
    },
    assignedBy:{
        type: String,
        required: [true, "Please provide username"]
    },
    details:{type:String}
})

export default mongoose.model('extUser',ExtSchema);