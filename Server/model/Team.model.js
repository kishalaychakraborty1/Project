import mongoose from "mongoose";
import  validators  from "mongoose-validators";
export const TeamSchema = new mongoose.Schema({
    teamName: {
        type: String,
        required: [true, "Please provide your team name"]
    },
    details: { type: String,required:[true,"please provide the description of the team"] },
    pincode: { type: Number,validate:validators.isLength(6),required:[true,"Please provide the pincode"]},
    purpose: { type: String},
    orgId: { type: String, required: [true, "Please provide your organization Id"], },
    orgId: { type: String },
    status:{
        type: String,
        enum: ['WOK','RMV','TRS'],
        required:[true,"Please provide the status properly"]
    },
    guestTeam: { type: Boolean, required: [true, "Please provide the field"] },
    username: { type: String, required: [true, "Please provide your username"], },
    Creation_date: { type: Date, default: Date.now },
    userModified: { type: String,default:""},
    Modified_date: { type: Date, default: Date.now }
})

export default mongoose.model('Team', TeamSchema)