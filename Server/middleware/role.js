
export const roleViaPosition = (position) => {
    
    if (position == "TEM" || position == "CLT" || position == "EXE" || position == "NOU") return [];

    else if (position == "TLD") return ['TLD', 'NOU', 'TEM', 'CLT', 'EXE']

    else if (position == "MNG") ['ADM', 'MNG', 'NOU', 'TLD', 'TEM', 'CLT', 'EXE']

    else if (position == "ADM") return ['ADM', 'MNG', 'NOU', 'TLD', 'TEM', 'CLT', 'EXE']

    else return null;
}