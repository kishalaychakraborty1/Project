import TeamMember from "../model/TeamMember.model.js"

export const uniqueUsernameForTeamId = async (req,res,next) => {
    try {
        //this is use -- when AKASH is present in usermodel with role MNG
        // -- but when a username of Ext-user or non-user is used by admin to add in model which is already present in the db 
        const {username} = req.body;
        const {TeamId} = req.params;
        const oldUser = await TeamMember.findOne({username,TeamId})
        if(oldUser) return res.status(500).send("User is already present in the same team")
        next()
    } catch (error) {
        return res.status(404).send("Something wrong")
    }
}