import JWT from 'jsonwebtoken'
import ENV from '../config.js'

export const Auth = async (req, res, next) => {
    try {
        //access authorize header to validate request
        const token = req.headers.authorization.split(" ")[1]
        const decodeToken = await JWT.verify(token, ENV.JWT_SECRET)
        console.log(decodeToken)
        // get userId from the decodeToken
        req.user = decodeToken;
        // console.log(req.user)
        next()
    } catch (error) {
        return res.status(401).json({ error: "Authorizatin fail" })
    }
}

export const localVariable = async (req,res,next) =>{
        req.app.locals = {
        OTP:null,
        resetSession :false
    }
     
    next()
}