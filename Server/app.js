import express from "express";
import cors from 'cors'
import morgan from 'morgan'
const app = express();
import connect from "./db/connect.js";
import {router} from './router/auth.js'
import { routerOrg } from "./router/org.js";
import { teamRouter } from "./router/team.js";

//middleware
app.use(express.json())
app.use(cors())
app.use(morgan('tiny'))
app.disable('x-powered-by')


// api routes 
app.use('/api',router);
app.use('/api',routerOrg);
app.use('/api',teamRouter)

const port = 8000;

//start the server when the db is connected
connect().then(() => {
    app.get('/', (req, res) => {
        res.status(200).json("Server started at port " + port);
    })
    app.listen(port, (req,res) => {
        console.log("Listening")
    })
}).catch(e => console.log(e));
