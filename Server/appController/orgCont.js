import Organization from '../model/Org.model.js'
import User from '../model/User.model.js'
import OrgMember from '../model/OrgMember.model.js'
import ExtUser from '../model/Ext.model.js'
import * as roleFxn from '../middleware/role.js'

// middleware to check orgId is there or not
export const verifyOrg = async (req, res, next) => {
    try {

        const { orgId } = req.params;
        const oldOrg = await Organization.findOne({ _id: orgId });
        if (!oldOrg) return res.status(404).send("Org has n't been found")
        next()
    } catch (error) {
        return res.status(404).send("Error while verifying the org")
    }
}

export const createOrgMiddle = async (req, res, next) => {
    try {
        const { userId } = req.params
        const newOrg = new Organization({ created_by: userId, ...req.body })
        newOrg.save()
            .then((data) => {
                if (!data) return res.status(401).send("Something wrong while saving the user details");
                req.app.locals.akash = data;
                next()
            })
            .catch(e => res.status(401).send({ e } + "Enable to create the org"));

    } catch (error) {
        return res.status(404).send({ error });
    }
}
export const createOrg = async (req, res) => {
    try {
        //create a addMember call as a admin to the user
        const local = req.app.locals.akash
        const user = await User.findOne({ _id: local.created_by })
        console.log(local)
        const newAdmin = new OrgMember({ orgId: local._id, newMember: user.username, assignedBy: user.username, role: "ADM" })
        console.log("object")
        if (!newAdmin) return res.status(400).send("Error while creating the admin in the orgMember");
        newAdmin.save().then(data => {
            if (!data) return res.status(404).send("Error while saving the data");
            return res.status(200).send("admin added success");
        })
    } catch (error) {
        return res.status(404).send({ error });
    }
}


export const updateOrg = async (req, res) => {
    try {
        const { orgId } = req.params
        const condition = { _id: orgId }
        Organization.updateOne(condition
            , req.body)
            .then(result => {
                if (!result) return res.status(501).send("Organization is not upadated");
                return res.status(200).send("Organization updated");
            })
    } catch (error) {
        return res.status(404).send(error);
    }
}

export const deleteOrg = async (req, res) => {
    try {
        const { orgId } = req.params
        const data = await Organization.findOne({ _id: orgId })
        if (data) {
            Organization.deleteOne({ _id: orgId })
                .then(data => {
                    if (!data) return res.status(401).send("Org is not deleted....something wrong");
                    return res.status(200).send("Organization has been deleted");
                })
        }
        else res.status(401).send("ID nor found");
    } catch (error) {
        return res.status(404).send({ error })
    }
}

//get all the organization created by a specific user
export const getAllOrganization = async (req, res) => {
    try {
        const { userId } = req.params;
        Organization.find({ created_by: userId }).then(result => {
            return res.status(200).send(result)
        }).catch(error => res.status(404).send("Can't find the organization"))
    } catch (error) {
        return res.status(404).send({ error: "Error while finding the organization" });
    }
}


// middleware to check | orgId is present in the org | and the created_by for that perticular org should mathes with assigned_by of the orgMemeber

export const verfiyOrgUser = async (req, res, next) => {
    try {
        const { orgId } = req.params;
        const { assignedBy } = req.body;

        const user = await User.findOne({ username: assignedBy });
        if (!user) return res.status(404).send("Such user is not exist");

        const org = await Organization.findOne({ _id: orgId });
        if (org.created_by.toString() != user._id.toString()) return res.status(404).send("No such who create that org")

        next()
    } catch (error) {
        return res.status(404).send("Somthing wrong while adding the memeber")
    }
}




// localhost:8000/api/assignRoleOrg/:orgId
// export const assignRoleOrg = async (req, res) => {
//     try {
//         const { orgId } = req.params

//         const { newMember, role } = req.body;

//         if (role === 'ADM' || role === 'MNG' || role === 'TLD' || role === 'TEM') {
//             const oldSignUp = await User.findOne({ username: newMember })
//             if (oldSignUp.isExternal != false) return res.status(404).send("NewMember is not found in the user model")
//         }
//         if (role === 'NOU' || role === 'CLT' || role === 'EXE') {
//             const oldExtUser = await User.findOne({ username: newMember })
//             if (oldExtUser.isExternal != true) return res.status(404).send("NewMember is not found in the user model")
//         }
//         // a user in the same org can't present in more than one role
//         const oldUser = await OrgMember.findOne({ orgId, newMember })

//         if (oldUser) return res.status(404).send("This is already exsit in this schema")

//         const newOrgMember = new OrgMember({ ...req.body, orgId })
//         newOrgMember.save()
//             .then((data) => {
//                 return res.status(200).send("User has added to the org");
//             })
//             .catch(e => res.status(401).send({ e } + "Enable to add the user in the org"));

//     } catch (error) {
//         return res.status(404).send({ error });
//     }
// }

export const roleOrgMiddle = async (req, res, next) => {
    try {
        //user who create the org
        const { orgId } = req.params;
        var { newMember, assignedBy, role } = req.body
        if (newMember == undefined) {
            newMember = req.params.newMember
        }
        console.log(newMember)
        const senior = await OrgMember.findOne({ orgId, newMember: assignedBy })

        // console.log(senior)
        var seniorLevel = senior.role;
        // console.log(seniorLevel)
        const juniorRole = roleFxn.roleViaPosition(seniorLevel);
        console.log(juniorRole)
        if (role != undefined) {
            if (juniorRole.includes(role)) {
                console.log("XXX1")
                if (role === 'ADM' || role === 'MNG' || role === 'TLD' || role === 'TEM') {
                    const oldSignUp = await User.findOne({ username: newMember })
                    if (oldSignUp.isExternal == true) return res.status(404).send("The external user can't be set as " + role)
                }
                if (role === 'NOU' || role === 'CLT' || role === 'EXE') {
                    console.log("XXX")
                    const oldExtUser = await User.findOne({ username: newMember })
                    if (oldExtUser.isExternal == false) return res.status(404).send("The normal user can't be set as " + role)
                }
                // a user in the same org can't present in more than one role

                next()
            }
            else {
                return res.status(404).send("maintain the level of authorization of role")
            }
        }
        else {
            next()
        }
    } catch (error) {
        return res.status(404).send({ error });
    }
}

export const assignRoleOrg = async (req, res, next) => {
    try {
        const { orgId } = req.params;
        const { newMember } = req.body;
        const oldUser = await OrgMember.findOne({ orgId, newMember })

        if (oldUser) return res.status(404).send("This is already exsit in this schema")
        const newOrgMember = new OrgMember({ ...req.body, orgId })
        newOrgMember.save()
            .then((data) => {
                return res.status(200).send("User has added to the org");
            })
            .catch(e => res.status(401).send({ e } + "Enable to add the user in the org"));
    } catch (error) {
        return res.status(404).send(error)
    }
}

// update the member in the org
// add newMember to the body
export const addNewMemberRole = async (req, res, next) => {
    try {
        const { orgId, newMember } = req.params

        const user = await OrgMember.findOne({ orgId, newMember });
        console.log(user)
        if (!user) return req.status(404).send("Error while finding the user")
        // if assignedby is not mentioned then default is the who create the org
        if (req.body.assignedBy == undefined)
            req.body.assignedBy = user.assignedBy
        if (req.body.role == undefined)
            req.body.role = user.role
        next()
    } catch (error) {
        return res.status(404).send({ error })
    }
}
//if you want u can set assignedby value and if u don't set the role then will automatically set role to previous one so after update no change in the roles
export const updateRoleOrg = async (req, res) => {
    try {
        const { orgId, newMember } = req.params;
        const find = await OrgMember.findOne({ orgId, newMember })
        if (find) {
            const condition = { orgId, newMember };
            OrgMember.updateOne(condition, req.body)
                .then(result => {
                    if (!result) return res.status(501).send("New member in the org is not upadated");
                    return res.status(200).send("New member in the org  updated");
                })
        }
        else return req.status(404).send("User in the role schema not find")
    } catch (error) {
        return res.status(404).send(error);
    }
}


// if u need u can set assignedBy and role value at the body
export const deleteRoleOrg = async (req, res) => {
    try {
        const { orgId, newMember } = req.params
        const find = await OrgMember.findOne({ orgId, newMember })
        if (find) {
            OrgMember.deleteOne({ orgId, newMember })
                .then(data => {
                    if (!data) return res.status(401).send("New member in the org is not deleted....something wrong");
                    return res.status(200).send("New member in the org has been deleted");
                })
        }
        else return req.status(404).send("User in the role schema not find")
    } catch (error) {
        return res.status(404).send({ error })
    }
}