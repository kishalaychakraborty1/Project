import UserModel from "../model/User.model.js"
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import ENV from '../config.js'
import otpGenerator from 'otp-generator'
import { localVariable } from '../middleware/auth.js'
//middleware for verify user


//For normal User only
export const verifyUser = async (req, res, next) => {
    try {
        //console.log("object")
        const { username } = req.body;
        const { userId } = req.params
        var exist;

        if (userId == undefined)
             exist = await UserModel.findOne({ username });

        if (username == undefined)
             exist = await UserModel.findOne({ _id:userId });

        
        if (!exist) return res.status(404).send({ error: "Can't find the user" })
        if (exist.isExternal === true) return res.status(404).send({ error: "Can't access the website" })

        next()
    } catch (error) {
        return res.status(404).send({ error: "Authenticate Error" });
    }
}



export const register = async (req, res) => {
    try {
        //isExternal == true password is not required
        //existing user

        const { username, password, email, isExternal } = req.body

        const oldUsername = new Promise((resolve, reject) => {
            UserModel.findOne({ username }).then((data) => {
                return reject(new Error(err))
            })
                .catch(e => reject({ error: e }))

            resolve();
        })

        const oldEmail = new Promise((resolve, reject) => {
            UserModel.findOne({ email }).then((data) => {
                return reject(new Error(err))
            })
                .catch(e => reject({ error: "This email is already used" }))

            resolve();
        })

        Promise.all([oldUsername, oldEmail])
            .then(() => {
                if (password && isExternal === false) {
                    bcrypt.hash(password, 10).then(hashPass => {
                        console.log(hashPass)
                        delete req.body['password']
                        console.log(req.body)
                        // create new USer and store in the db
                        const newUser = new UserModel({ ...req.body, password: hashPass })
                        console.log(newUser)
                        newUser.save().then(result => {
                            res.status(200).send("User added succesfully")
                        }).catch(e => {
                            res.status(500).send(e)
                        })

                    }).catch(e => {
                        return res.status(500).send({ error: "Error in hashing" })
                    })
                }
                if (isExternal === true) {
                    if (password) {
                        console.log("For external user password is not required");
                    }
                    console.log("XXX")
                    delete req.body['password']
                    const newExtUser = new UserModel(req.body)
                    console.log(newExtUser)
                    newExtUser.save().then(result => {
                        res.status(200).send("User added succesfully")
                    }).catch(e => {
                        res.status(500).send(e)
                    })
                }
            })
            .catch(e => {
                return res.status(500).send({ error: "Enable to hashed pass " + e })
            })

    } catch (error) {
        return res.status(500).send(error)
    }
}


export const login = async (req, res) => {
    try {
        const { username, password } = req.body
        UserModel.findOne({ username }).then((user) => {
            if (user.isExternal == false) {
                bcrypt.compare(password, user.password).then(passCheck => {
                    if (!passCheck) return res.status(500).send({ error: "Password not found" })

                    // after proper login create jwt token
                    const token = jwt.sign({
                        userId: user._id,
                        username: user.username
                    }, ENV.JWT_SECRET, { expiresIn: '24h' })

                    return res.status(200).send({
                        msg: "Login Successfully..",
                        username: user.username,
                        token
                    })

                }).catch(e => {
                    return res.status(500).send({ error: "Password not match" })
                })
            }
            else return res.status(404).send("can't access the website")
        })

    } catch (error) {
        return res.status(500).send({ error: "User not found" })
    }
}

export const getUser = async (req, res) => {
    //fetch the dynamic value of username from the url
    const { userId } = req.params;
    try {
        if (!userId) return res.status(501).send({ error: "Invalid userI" });
        UserModel.findOne({ _id: userId }).then(data => {
            if (!data) return res.status(501).send({ error: "Couldn't find the user" })

            //convert mongoose data -> json and store it to the empty object
            const { password, ...rest } = Object.assign({}, data.toJSON());
            return res.status(201).send(rest)
        })
    } catch (error) {
        return res.status(501).send(error)
    }
}

export async function updateUser(req, res) {
    try {

        const { userId } = req.user;
        console.log(userId)
        const condition = { _id: userId }
        // const { userId } = req.user;
        if (userId) {
            // const body = req.body;

            // update the data 
            UserModel.updateOne(condition, req.body).then((data) => {
                if (!data) return res.status(404).send("Fuck");
                return res.status(200).send("OK")
            }).catch(e => {
                return res.status(401).send("Error while updating")
            })

        } else {
            return res.status(401).send({ error: "User Not Found...!" });
        }

    } catch (error) {
        return res.status(401).send({ error: "jko" });
    }
}

export const updateExtUser = async (req,res) =>{
    try {
      const {userId} = req.params;
      const oldUser = await UserModel.findOne({_id:userId})
      if(!oldUser) return res.status(404).send("User has not found");
      UserModel.updateOne({_id:userId},req.body)
      .then(data=>{
        if(!data)   return res.status(404).send("UserExt has not been updated");
        return res.status(200).send("User has been updated");
      })
    } catch (error) {
      return res.status(404).send({error});
    }
}


export const generateOTP = async (req, res) => {
    //this otp must access by the verifyOTP so we use middleware to do that
    req.app.locals.OTP = await otpGenerator.generate(6, { lowerCaseAlphabets: false, upperCaseAlphabets: false, specialChars: false })
    console.log(req.app.locals.OTP)
    res.status(201).send({ code: req.app.locals.OTP });
}
export const verifyOTP = async (req, res) => {
    const { OTP } = req.body
    if (parseInt(req.app.locals.OTP) == parseInt(OTP)) {
        req.app.locals.OTP = null;
        req.app.locals.resetSession = true
        return res.status(201).send(req.app.locals)
    }
    return res.status(400).send({ error: "Invalid OTP" })
}

// export const createResetSession = async (req, res,next) => {
//     if (req.app.locals.resetSession == true) {
//         req.app.locals.resetSession = false;
//         next()
//     }
//     return res.status(440).send({ error: "SEssion has been expired" });
// }
export const resetPassword = async (req, res) => {
    console.log(req.app.locals.resetSession)
    if (!req.app.locals.resetSession) return res.status(440).send({ error: "SEssion has been expired" });

    try {
        const {userId} = req.params
        const {password } = req.body
        const hashNewPass = await bcrypt.hash(password, 10)
        UserModel.updateOne({ _id: userId }, { password: hashNewPass })
            .then(x => {
                req.app.locals.resetSession = false
                res.status(201).send("Password has  been updated")
            })
            .catch(e => res.status(404).send(e));
    } catch (error) {
        return res.status(404).send(error);
    }

}

// First of all -> Generate the OTP
// sencondly -> Verify the OTP
//           -> Get the session
//           -> Change Password before the session has expired
