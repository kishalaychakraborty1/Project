import nodemailer from 'nodemailer'
import Mailgen from 'mailgen'
import ENV from '../config.js'
import UserModel from '../model/User.model.js';

let transporter = nodemailer.createTransport({
    host: "smtp.ethereal.email",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: ENV.USER, // generated ethereal user
      pass: ENV.PASS, // generated ethereal password
    },
  });

  let MailGenerator = new Mailgen({
    theme:"neopolitan", 
    textDirection: 'rtl',
    product:{
        name:"Mailgen",
        link:'https://mailgen.js/'
    } 
  })



export const registerMail = async (req,res) =>{
  const  {userEmail,text,subject,username} = req.body
  //body
  const {userId} = req.params;
  
  const user = await UserModel.findOne({_id:userId})
  if(!user) return res.status(404).send("User has not been found") 


  var email = {
    body:{
      name : username,
      intro : text || "welcome ",
      outro : "Need some more "
    }
  }
  var emailBody = await MailGenerator.generate(email)

  let message = {
    from : ENV.USER,
    to : userEmail,
    subject : subject || "sign up successfully",
    html : emailBody
  }
  //send mail
  transporter.sendMail(message)
  .then(()=>{
    return res.status(201).send("You have received an email form us");
  })
  .catch(e=>{
    return res.status(500).send({e});
  })
}