import Team from "../model/Team.model.js";
import Organization from "../model/Org.model.js";
import TeamMember from "../model/TeamMember.model.js";
import User from '../model/User.model.js'

export const orgIdPresent = async (req, res, next) => {
    const { orgId } = req.method == 'GET' ? req.params : req.body;
    try {
        Organization.findOne({ _id: orgId })
            .then(result => {
                if (!result) return res.status(404).send("OrgId has not been found")
                console.log("OrgId has been found")
                next()
            })
            .catch(e => {
                return res.status(401).send("orgId is invalid");
            })
    } catch (error) {
        return res.status(404).send("error")
    }
}

export const createTeam = async (req, res) => {
    try {
        const { teamName, details, pincode, status, guestTeam, username, orgId } = req.body;

        // if (!teamName || !details || !pincode || !status || !guestTeam || !username || !orgId) return res.status(401).send("Please provide all the requied field")

        const newTeam = new Team(req.body);
        newTeam.save().then(result => {
            if (!result) return res.status(401).send("Team hsa not been created");
            return res.status(200).send("Team has been created");
        })
    }
    catch (error) {
        return res.status(404).send(error)
    }
}

export const updateTeam = async (req, res) => {
    try {
        // orgId and _id is must be present in the body
        const findId = await Team.findOne({ _id: req.body._id });
        if (findId) {
            const condition = { _id: req.body._id, orgId: req.body.orgId }
            Team.updateOne(condition, req.body)
                .then(result => {
                    if (!result) res.status(201).send("Error in updating the team");
                    return res.status(200).send("Team has been upadated");
                })
                .catch(e => res.status(404).send("No such team has been found"))
        }
        else return res.status(404).send("Team not found")
    } catch (error) {
        return res.status(404).send({ error: "Something wrong while updating the team" });
    }
}

export const deleteTeam = async (req, res) => {
    try {
        const findId = await Team.findOne({ _id: req.body._id });
        if (findId)
            Team.deleteOne({ _id: req.body._id })
                .then(result => {
                    if (!result) return res.status(401).send("Error while deleting the team")
                    return res.status(200).send("team has been deleted");
                })
        else return res.status(404).send("Team not found")
    } catch (error) {
        return res.status(404).send({ error: "Something wrong while deleting the team" })
    }
}
// get all the team name via username or via orgId

export const getAllTeam = async (req, res) => {
    try {
        //getting team via username
        const { username, orgId } = req.query;

        if (orgId == undefined) {
            const organizations = await Organization.find({ userCreated: username }).exec();
            if (organizations.length === 0) {
                return res.status(404).json({ message: 'Organizations not found' });
            }

            const orgIds = organizations.map(org => org._id);
            console.log(orgIds)
            var teams = await Team.find({ orgId: { $in: orgIds } }).exec();
            if (teams.length === 0) {
                return res.status(404).json({ message: 'Teams not found' });
            }
        }


        //getting team via orgId

        if (username == undefined) {
            try {
                var teams = await Team.find({ orgId })
                if (teams.length === 0) res.status(401).send("No teams has been found");
            } catch (error) {
                return res.status(404).send({ error: "Error while finding the team" })
            }
        }

        return res.send({ teams });
    } catch (error) {
        console.error('Error finding teams:', error);
        res.status(500).json({ message: 'Internal server error' });
    }

};

//assign the role in the team

// localhost:8000/api/assignRoleTeam/:TeamId
export const assignRoleTeam = async (req, res) => {
    try {
        const { Role, username } = req.body;
        if (Role === 'ADM' || Role === 'MNG' || Role === 'TLD' || Role === 'TEM') {
            // check if they have the username present in the user model
            const oldUser = await User.findOne({ username })
            if (!oldUser) return res.status(404).send("User is not found in the user model")
        }
        
        const findId = await TeamMember.findOne({ TeamId: req.params.TeamId, username, Role });
        if (findId) return res.status(401).send("User is already added at that team with the same role");
        const newRoleTeam = new TeamMember({ ...req.body, TeamId: req.params.TeamId });
        console.log("object")
        newRoleTeam.save().then(result => {
            if (!result) return res.status(401).send("Team member role has not been created");
            return res.status(200).send("Team member role has been created");
        })
        
        
    }
    catch (error) {
        return res.status(404).send("error")
    }
}

// localhost:8000/api/updateRoleTeam/:TeamId
export const updateRoleTeam = async (req, res) => {
    try {
        // orgId and _id is must be present in the body
        const { Role, username } = req.body;
        const {TeamId} = req.params
        if (Role === 'ADM' || Role === 'MNG' || Role === 'TLD' || Role === 'TEM') {
            // check if they have the username present in the user model
            const oldUser = await User.findOne({ username })
            if (!oldUser) return res.status(404).send("User is not found in the user model")
        }
        // console.log("object")
        const findId = await TeamMember.findOne({  TeamId, username  }); 
        if (findId) {
            const condition = { TeamId, username }
            TeamMember.updateOne(condition, req.body)
            .then(result => {
                if (!result) res.status(201).send("Error in updating the Team member role");
                return res.status(200).send("Team member role has been upadated");
            })
            .catch(e => res.status(404).send("No such Team member role has been found"))
        }
        else return res.status(404).send("Team not found")
    } catch (error) {
        return res.status(404).send({ error: "Something wrong while updating the Team member role" });
    }
}

// localhost:8000/api/deleteRoleTeam/:TeamId
export const deleteRoleTeam = async (req, res) => {
    try {
        const {TeamId} = req.params
        console.log("object")
        const { Role, username } = req.body;
        if (Role === 'ADM' || Role === 'MNG' || Role === 'TLD' || Role === 'TEM') {
            // check if they have the username present in the user model
            const oldUser = await User.findOne({ username })
            if (!oldUser) return res.status(404).send("User is not found in the user model")
        }
        //pass TeamId and username
        const findId = await TeamMember.findOne({ TeamId,Role, username })
        if (findId)
            TeamMember.deleteOne({ TeamId, username,Role })
                .then(result => {
                    if (!result) return res.status(401).send("Error while deleting the Team member role")
                    return res.status(200).send("Team member role has been deleted");
                })
        else return res.status(404).send("Team member role not found")
    } catch (error) {
        return res.status(404).send({ error: "Something wrong while deleting the Team member role" })
    }
}