import { Router } from "express";
const teamRouter = Router()
// import * as Controller from '../appController/authCont'
import * as teamContoller from '../appController/teamCont.js'
import * as teamMemMiddle from '../middleware/team.js'


teamRouter.route('/createTeam').post(teamContoller.orgIdPresent,teamContoller.createTeam)
teamRouter.route('/updateTeam').put(teamContoller.orgIdPresent,teamContoller.updateTeam)
teamRouter.route('/deleteTeam').delete(teamContoller.orgIdPresent,teamContoller.deleteTeam)

teamRouter.route('/getAllTeam').get(teamContoller.getAllTeam)

teamRouter.route('/assignRoleTeam/:TeamId').post(teamMemMiddle.uniqueUsernameForTeamId,teamContoller.assignRoleTeam)
teamRouter.route('/updateRoleTeam/:TeamId').put(teamContoller.updateRoleTeam)
teamRouter.route('/deleteRoleTeam/:TeamId').delete(teamContoller.deleteRoleTeam)

export {teamRouter} 