import { Router } from "express";
const routerOrg = Router()
import * as controller from '../appController/authCont.js'
import * as orgController from '../appController/orgCont.js'


routerOrg.route('/createOrg/:userId').post(controller.verifyUser, orgController.createOrgMiddle, orgController.createOrg)
routerOrg.route('/updateOrg/:orgId').put(orgController.verifyOrg, orgController.updateOrg)
routerOrg.route('/deleteOrg/:orgId').delete(orgController.verifyOrg, orgController.deleteOrg)

routerOrg.route('/getAllOrganization/:userId').get(controller.verifyUser, orgController.getAllOrganization)

routerOrg.route('/assignRoleOrg/:orgId').post(orgController.verfiyOrgUser, orgController.roleOrgMiddle, orgController.assignRoleOrg)

routerOrg.route('/updateRoleOrg/:orgId/:newMember').put(orgController.addNewMemberRole, orgController.verfiyOrgUser, orgController.roleOrgMiddle, orgController.updateRoleOrg)

routerOrg.route('/deleteRoleOrg/:orgId/:newMember').delete(orgController.addNewMemberRole, orgController.verfiyOrgUser, orgController.roleOrgMiddle, orgController.deleteRoleOrg)

export { routerOrg }