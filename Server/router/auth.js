import {Router} from 'express'
const router = Router()
import * as controller from '../appController/authCont.js'
import {Auth,localVariable} from '../middleware/auth.js'
import { registerMail } from '../appController/mailer.js'
//post
router.route('/authenticate').post(controller.verifyUser)
router.route('/login').post(controller.verifyUser,controller.login)
router.route('/register').post(controller.register)

//mail
router.route('/registerMail/:userId').post(registerMail)


//get
router.route('/user/:userId').get(controller.getUser);
router.route('/generateOTP/:userId').get(controller.verifyUser,localVariable,controller.generateOTP);
router.route('/verifyOTP/:userId').post(controller.verifyUser,controller.verifyOTP);

 
//put
router.route('/updateUser').put(Auth,controller.updateUser);
router.route('/resetPassword/:userId').put(controller.verifyUser,controller.resetPassword);

//update Extuser
router.route('/updateExtUser/:userId').put(controller.updateExtUser)

export {router} 