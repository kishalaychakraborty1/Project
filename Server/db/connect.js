import mongoose from "mongoose";
import ENV from '../config.js'
const DB = `mongodb+srv://${ENV.MONGO_USER}:${ENV.MONGO_PASS}@cluster0.8js6gir.mongodb.net/Product_management?retryWrites=true&w=majority`



const connect = async () => {
    const database = await mongoose.connect(DB, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    console.log("DB connected")
    return database;
}
export default connect
